!include LogicLib.nsh

!define PRODUCT_NAME "Seafile Updater"
!define PRODUCT_VERSION "1.0"

!define URL_LATEST "http://example.com/latest.txt"
!define URL_FILE "http://example.com/file.txt"

Name "${PRODUCT_NAME}"
Caption "${PRODUCT_NAME}"
Icon "seafile_updater.ico"
OutFile "updater.exe"

RequestExecutionLevel admin

Var InstalledProductCode
Var InstalledVersion
Var LatestVersion
Var SeafileURL
Var TempDir

# Checks result of inetc::* calls, aborting when error occurred
!macro CheckOK
	Pop $0
	StrCmp $0 "OK" +2 0
	Abort "Download failed!"
!macroend

# Checks exit code (assumed to be in $0) of a command.
# Aborts with ${ErrorString} if it is non-zero.
!macro CheckExitCode ErrorString
	StrCmp $0 0 +2 0
	Abort "${ErrorString} Exit code: $0"
!macroend

# Reads content of file ${FileName} to variable ${Variable}
!macro ReadToVariable FileName Variable
	FileOpen $0 "${FileName}" r
	FileRead $0 $1
	FileClose $0
	Push $1
	Pop ${Variable}
!macroend

# Downloads contents of file at ${URL} to variable ${Variable}
# using temporary file ${FileName}
!macro DownloadToVariable URL FileName Variable
	inetc::get "${URL}" "${FileName}"
	!insertmacro CheckOK
	!insertmacro ReadToVariable "${FileName}" ${Variable}
	Delete "${FileName}"
!macroend

# see http://nsis.sourceforge.net/VersionCompare
Function VersionCompareExec
	!define VersionCompare `!insertmacro VersionCompareCall`
 
	!macro VersionCompareCall _VER1 _VER2 _RESULT
		Push `${_VER1}`
		Push `${_VER2}`
		Call VersionCompareExec
		Pop ${_RESULT}
	!macroend
 
	Exch $1
	Exch
	Exch $0
	Exch
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $7
 
	begin:
	StrCpy $2 -1
	IntOp $2 $2 + 1
	StrCpy $3 $0 1 $2
	StrCmp $3 '' +2
	StrCmp $3 '.' 0 -3
	StrCpy $4 $0 $2
	IntOp $2 $2 + 1
	StrCpy $0 $0 '' $2
 
	StrCpy $2 -1
	IntOp $2 $2 + 1
	StrCpy $3 $1 1 $2
	StrCmp $3 '' +2
	StrCmp $3 '.' 0 -3
	StrCpy $5 $1 $2
	IntOp $2 $2 + 1
	StrCpy $1 $1 '' $2
 
	StrCmp $4$5 '' equal
 
	StrCpy $6 -1
	IntOp $6 $6 + 1
	StrCpy $3 $4 1 $6
	StrCmp $3 '0' -2
	StrCmp $3 '' 0 +2
	StrCpy $4 0
 
	StrCpy $7 -1
	IntOp $7 $7 + 1
	StrCpy $3 $5 1 $7
	StrCmp $3 '0' -2
	StrCmp $3 '' 0 +2
	StrCpy $5 0
 
	StrCmp $4 0 0 +2
	StrCmp $5 0 begin newer2
	StrCmp $5 0 newer1
	IntCmp $6 $7 0 newer1 newer2
 
	StrCpy $4 '1$4'
	StrCpy $5 '1$5'
	IntCmp $4 $5 begin newer2 newer1
 
	equal:
	StrCpy $0 0
	goto end
	newer1:
	StrCpy $0 1
	goto end
	newer2:
	StrCpy $0 2
 
	end:
	Pop $7
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Exch $0
FunctionEnd

Section
	HideWindow
	
	# Check if another copy is running - if yes, abort
	System::Call 'kernel32::CreateMutex(i 0, i 0, t "SeafileUpdaterMutex") ?e'
	Pop $R0
	StrCmp $R0 0 not-running
		DetailPrint "The installer is already running."
		Quit
	not-running:
	
	# Create temporary directory
	StrCpy $TempDir "$TEMP\${PRODUCT_NAME}"
	CreateDirectory $TempDir
	
	# Read installed Seafile version
	# Upgrade code: {65DED1C8-A5F1-4C49-8E7E-B0A8A5A6535C}
	EnumRegValue $1 HKLM "Software\Classes\Installer\UpgradeCodes\8C1DED561F5A94C4E8E70B8A5A6A35C5" 0
	IfErrors 0 seafile-installed-1
		DetailPrint "Seafile is not installed on this computer! (code 1)"
		Quit
	seafile-installed-1:
	StrCpy $InstalledProductCode $1
	
	ReadRegStr $1 HKLM "Software\Classes\Installer\Products\$InstalledProductCode" "ProductName"
	IfErrors 0 seafile-installed-2
		DetailPrint "Seafile is not installed on this computer! (code 2)"
		Quit
	seafile-installed-2:
	StrCpy $InstalledVersion "$1" "" 8
	
	DetailPrint "Currently installed Seafile version: $InstalledVersion"
	
	# Download information about latest version
	!insertmacro DownloadToVariable "${URL_LATEST}" "$TempDir\latest.txt" $LatestVersion
	DetailPrint "Latest Seafile version available: $LatestVersion"
	
	# Check if upgrade is needed
	${VersionCompare} "$InstalledVersion" "$LatestVersion" $R1
	${If} $R1 < 2
		DetailPrint "No update needed"
		Quit
	${EndIf}
	
	# Ask user whether to update automatically
	MessageBox MB_YESNO|MB_ICONQUESTION "A new version of Seafile is available. Do you want to update to Seafile $LatestVersion now?$\n$\nMake sure that Seafile is turned off before clicking Yes." IDYES update-now
		DetailPrint "Seafile update cancelled by the user."
		Quit
	
	update-now:
	BringToFront
	
	# Get URL to *.msi installer
	DetailPrint "Updating Seafile..."
	!insertmacro DownloadToVariable "${URL_FILE}" "$TempDir\file.txt" $SeafileURL
	
	# Download MSI file
	DetailPrint "Downloading $SeafileURL..."
	inetc::get "$SeafileURL" "$TempDir\seafile.msi"
	
	# Execute msiexec
	DetailPrint "Installing Seafile..."
	ExecWait 'msiexec /i "$TempDir\seafile.msi" /qb /passive /norestart' $0
	!insertmacro CheckExitCode "Seafile update failed."
	
	# Delete temporary files
	Delete "$TempDir\seafile.msi"
	DetailPrint "Seafile updated succesfully to version $LatestVersion!"
	Quit
SectionEnd
